package com.example.bikelife.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikelife.R;
import com.example.bikelife.model.Bike;

import java.util.List;

public class BikeAdapter extends RecyclerView.Adapter<BikeAdapter.BikeViewHolder> {

    private List<Bike> bikeList;
    private Context context;

    public BikeAdapter(List<Bike> bikeList, Context context) {
        this.bikeList = bikeList;
        this.context = context;
    }

    @NonNull
    @Override
    public BikeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bike_item, parent, false);
        return new BikeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BikeViewHolder holder, int position) {
        Bike bike = bikeList.get(position);
        holder.bikeIdTextView.setText("ID: " + bike.getId());
        holder.bikeLocationTextView.setText("Lat: " + bike.getLatitude() + ", Lon: " + bike.getLongitude());

        holder.reserveButton.setOnClickListener(v -> {
            Toast.makeText(context, "Bike " + bike.getId() + " réservé!", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return bikeList.size();
    }

    static class BikeViewHolder extends RecyclerView.ViewHolder {
        TextView bikeIdTextView;
        TextView bikeLocationTextView;
        Button reserveButton;

        BikeViewHolder(View itemView) {
            super(itemView);
            bikeIdTextView = itemView.findViewById(R.id.bikeIdTextView);
            bikeLocationTextView = itemView.findViewById(R.id.bikeLocationTextView);
            reserveButton = itemView.findViewById(R.id.reserveButton);
        }
    }
}
