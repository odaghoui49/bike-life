package com.example.bikelife;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.bikelife.fragments.MyAccountFragment;
import com.example.bikelife.fragments.ReserveFragment;
import com.example.bikelife.fragments.MapFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        // Sélectionne l'onglet "Réserver" par défaut
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new ReserveFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    if (item.getItemId() == R.id.navigation_reserve) {
                        selectedFragment = new ReserveFragment();
                    } else if (item.getItemId() == R.id.navigation_scan_bike) {
                        selectedFragment = new MapFragment();
                    } else if (item.getItemId() == R.id.navigation_my_account) {
                        selectedFragment = new MyAccountFragment();
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();
                    return true;
                }
            };
}
