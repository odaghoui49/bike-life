package com.example.bikelife.repository;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bikelife.model.Bike;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BikeRepository {

    private static final String TAG = "BikeRepository";
    private OkHttpClient client = new OkHttpClient();
    private String baseUrl = "http://10.188.205.114:5000/";

    public interface BikeCallback {
        void onSuccess(List<Bike> bikeList);
        void onFailure(String errorMessage);
    }

    public void getAllBikes(BikeCallback callback) {
        String url = baseUrl + "bikes";
        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String responseData = response.body().string();
                        List<Bike> bikeList = parseBikeList(responseData);
                        callback.onSuccess(bikeList);
                    } catch (JSONException e) {
                        callback.onFailure("Error parsing JSON: " + e.getMessage());
                    }
                } else {
                    callback.onFailure("HTTP Error: " + response.code());
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Request failed: " + e.getMessage());
            }
        });
    }

    public void updateBike(int bikeId, double altitude, double longitude, BikeUpdateCallback callback) {
        UpdateBikeTask updateBikeTask = new UpdateBikeTask(bikeId, altitude, longitude, callback);
        updateBikeTask.execute();
    }

    private static class UpdateBikeTask extends AsyncTask<Void, Void, Void> {

        private int bikeId;
        private double altitude;
        private double longitude;
        private BikeUpdateCallback callback;

        public UpdateBikeTask(int bikeId, double altitude, double longitude, BikeUpdateCallback callback) {
            this.bikeId = bikeId;
            this.altitude = altitude;
            this.longitude = longitude;
            this.callback = callback;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // Construire le JSON de mise à jour du vélo
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("altitude", altitude);
                jsonBody.put("longitude", longitude);
                RequestBody requestBody = RequestBody.create(jsonBody.toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));

                // Effectuer la requête PUT
                String url = "http://10.188.205.114:5000/bikes/" + bikeId;
                Request request = new Request.Builder()
                        .url(url)
                        .put(requestBody)
                        .build();

                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();

                if (response.isSuccessful()) {
                    callback.onUpdateSuccess();
                } else {
                    callback.onUpdateFailure("HTTP Error: " + response.code());
                }
            } catch (IOException | JSONException e) {
                callback.onUpdateFailure("Request failed: " + e.getMessage());
            }
            return null;
        }
    }

    // Interface pour écouter le succès de la mise à jour du vélo
    public interface BikeUpdateCallback {
        void onUpdateSuccess();
        void onUpdateFailure(String errorMessage);
    }

    private List<Bike> parseBikeList(String jsonData) throws JSONException {
        List<Bike> bikeList = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(jsonData);
        JSONArray bikesArray = jsonObject.getJSONArray("bikes");
        for (int i = 0; i < bikesArray.length(); i++) {
            JSONObject bikeObject = bikesArray.getJSONObject(i);
            int id = bikeObject.getInt("id");
            double altitude = bikeObject.getDouble("altitude");
            double longitude = bikeObject.getDouble("longitude");
            Bike bike = new Bike(id, altitude, longitude);
            bikeList.add(bike);
        }
        return bikeList;
    }
}
