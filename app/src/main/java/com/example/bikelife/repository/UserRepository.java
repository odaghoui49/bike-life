package com.example.bikelife.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bikelife.database.DBHelper;
import com.example.bikelife.model.User;

public class UserRepository {

    private DBHelper dbHelper;

    public UserRepository(Context context) {
        dbHelper = new DBHelper(context);
    }

    // Méthode pour ajouter un utilisateur
    public long addUser(User user) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_USERNAME, user.getUsername());
        values.put(DBHelper.COLUMN_PASSWORD, user.getPassword());
        values.put(DBHelper.COLUMN_EMAIL, user.getEmail());
        values.put(DBHelper.COLUMN_PHONE_NUMBER, user.getPhoneNumber());
        values.put(DBHelper.COLUMN_PHOTO, user.getPhoto());
        long result = db.insert(DBHelper.TABLE_USERS, null, values);
        db.close();
        return result;
    }

    // Méthode pour vérifier si un utilisateur existe déjà
    public boolean checkUser(String username) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {DBHelper.COLUMN_USER_ID};
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        Cursor cursor = db.query(DBHelper.TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count > 0;
    }

    // Méthode pour vérifier les informations de connexion
    public boolean checkLogin(String username, String password) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {DBHelper.COLUMN_USER_ID};
        String selection = DBHelper.COLUMN_USERNAME + "=?" + " AND " + DBHelper.COLUMN_PASSWORD + "=?";
        String[] selectionArgs = {username, password};
        Cursor cursor = db.query(DBHelper.TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count > 0;
    }

    // Méthode pour obtenir les informations de l'utilisateur
    public User getUserInfo(String username) {
        User user = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {
                DBHelper.COLUMN_USERNAME,
                DBHelper.COLUMN_PASSWORD,
                DBHelper.COLUMN_EMAIL,
                DBHelper.COLUMN_PHONE_NUMBER,
                DBHelper.COLUMN_PHOTO
        };
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        Cursor cursor = db.query(DBHelper.TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int usernameIndex = cursor.getColumnIndex(DBHelper.COLUMN_USERNAME);
            int passwordIndex = cursor.getColumnIndex(DBHelper.COLUMN_PASSWORD);
            int emailIndex = cursor.getColumnIndex(DBHelper.COLUMN_EMAIL);
            int phoneNumberIndex = cursor.getColumnIndex(DBHelper.COLUMN_PHONE_NUMBER);
            int photoIndex = cursor.getColumnIndex(DBHelper.COLUMN_PHOTO);

            if (usernameIndex != -1 && passwordIndex != -1 && emailIndex != -1 && phoneNumberIndex != -1 && photoIndex != -1) {
                user = new User(
                        cursor.getString(usernameIndex),
                        cursor.getString(passwordIndex),
                        cursor.getString(emailIndex),
                        cursor.getString(phoneNumberIndex),
                        cursor.getBlob(photoIndex)
                );
            }
            cursor.close();
        }
        db.close();
        return user;
    }
    // Méthode pour mettre à jour les informations de l'utilisateur
    public boolean updateUserInfo(String username, String email, String phoneNumber, byte[] photo) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_EMAIL, email);
        values.put(DBHelper.COLUMN_PHONE_NUMBER, phoneNumber);
        values.put(DBHelper.COLUMN_PHOTO, photo);

        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};

        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }
    public boolean updateUsername(String oldUsername, String newUsername) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_USERNAME, newUsername);
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {oldUsername};
        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }

    public boolean updatePassword(String username, String newPassword) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_PASSWORD, newPassword);
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }

    public boolean updateEmail(String username, String newEmail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_EMAIL, newEmail);
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }

    public boolean updatePhoneNumber(String username, String newPhoneNumber) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_PHONE_NUMBER, newPhoneNumber);
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }

    public boolean updatePhoto(String username, byte[] newPhoto) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_PHOTO, newPhoto);
        String selection = DBHelper.COLUMN_USERNAME + "=?";
        String[] selectionArgs = {username};
        int count = db.update(DBHelper.TABLE_USERS, values, selection, selectionArgs);
        db.close();
        return count > 0;
    }

}
