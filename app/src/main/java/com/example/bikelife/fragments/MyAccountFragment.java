package com.example.bikelife.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bikelife.LoginActivity;
import com.example.bikelife.R;
import com.example.bikelife.model.User;
import com.example.bikelife.repository.UserRepository;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class MyAccountFragment extends Fragment {

    private EditText editTextUsername, editTextEmail, editTextPhoneNumber;
    private Button buttonUpdate, buttonLogout;
    private ImageView imageViewProfile;
    private UserRepository userRepository;
    private User currentUser;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_account, container, false);

        userRepository = new UserRepository(getContext());
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("username", "");
        currentUser = userRepository.getUserInfo(username);

        editTextUsername = rootView.findViewById(R.id.editTextUsername);
        editTextEmail = rootView.findViewById(R.id.editTextEmail);
        editTextPhoneNumber = rootView.findViewById(R.id.editTextPhoneNumber);
        buttonUpdate = rootView.findViewById(R.id.buttonUpdate);
        buttonLogout = rootView.findViewById(R.id.buttonLogout);
        imageViewProfile = rootView.findViewById(R.id.imageViewProfile);

        editTextUsername.setText(currentUser.getUsername());
        editTextEmail.setText(currentUser.getEmail());
        editTextPhoneNumber.setText(currentUser.getPhoneNumber());

        byte[] photoData = currentUser.getPhoto();
        if (photoData != null && photoData.length > 0) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(photoData, 0, photoData.length);
            imageViewProfile.setImageBitmap(bitmap);
        }

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserInfo();
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        return rootView;
    }

    private void updateUserInfo() {
        String email = editTextEmail.getText().toString().trim();
        String phoneNumber = editTextPhoneNumber.getText().toString().trim();
        byte[] photo = getPhotoData(); // Récupérer la photo à partir de la vue

        boolean emailUpdated = false;
        boolean phoneNumberUpdated = false;
        boolean photoUpdated = false;
        boolean usernameUpdated = false; // Nouvelle variable pour le nom d'utilisateur

        // Mettre à jour les champs individuellement
        if (!currentUser.getEmail().equals(email)) {
            emailUpdated = userRepository.updateEmail(currentUser.getUsername(), email);
        }

        if (!currentUser.getPhoneNumber().equals(phoneNumber)) {
            phoneNumberUpdated = userRepository.updatePhoneNumber(currentUser.getUsername(), phoneNumber);
        }

        if (photo != null && !Arrays.equals(photo, currentUser.getPhoto())) {
            photoUpdated = userRepository.updatePhoto(currentUser.getUsername(), photo);
        }

        if (!currentUser.getUsername().equals(editTextUsername.getText().toString().trim())) {
            usernameUpdated = userRepository.updateUsername(currentUser.getUsername(), editTextUsername.getText().toString().trim());
        }

        if (emailUpdated || phoneNumberUpdated || photoUpdated || usernameUpdated) {
            // Mettre à jour l'utilisateur actuel
            currentUser = new User(currentUser.getUsername(), currentUser.getPassword(), email, phoneNumber, photo);
            // Afficher un message de succès
            Toast.makeText(getContext(), "User information updated successfully", Toast.LENGTH_SHORT).show();
        } else {
            // Afficher un message d'échec
            Toast.makeText(getContext(), "No changes to update", Toast.LENGTH_SHORT).show();
        }
    }

    private byte[] getPhotoData() {
        // Convertir l'image du profil en tableau de bytes
        Drawable drawable = imageViewProfile.getDrawable();
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }
        // Retourner null si ce n'est pas un BitmapDrawable
        return null;
    }

    private void logout() {
        // Effacer les informations de connexion des SharedPreferences
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();

        // Rediriger vers LoginActivity
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish(); // Fermer l'activité actuelle pour terminer la session
    }
}
