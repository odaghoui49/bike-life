package com.example.bikelife.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikelife.R;
import com.example.bikelife.utils.BikeAdapter;
import com.example.bikelife.model.Bike;
import com.example.bikelife.repository.BikeRepository;

import java.util.List;

public class ReserveFragment extends Fragment {

    private RecyclerView bikesRecyclerView;
    private TextView noBikesTextView;
    private BikeRepository bikeRepository;
    private BikeAdapter bikeAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reserve, container, false);

        bikesRecyclerView = rootView.findViewById(R.id.bikesRecyclerView);
        noBikesTextView = rootView.findViewById(R.id.noBikesTextView);
        bikeRepository = new BikeRepository();

        bikesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        loadBikes();

        return rootView;
    }

    private void loadBikes() {
        bikeRepository.getAllBikes(new BikeRepository.BikeCallback() {
            @Override
            public void onSuccess(List<Bike> bikeList) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (bikeList.isEmpty()) {
                        noBikesTextView.setVisibility(View.VISIBLE);
                        bikesRecyclerView.setVisibility(View.GONE);
                    } else {
                        noBikesTextView.setVisibility(View.GONE);
                        bikesRecyclerView.setVisibility(View.VISIBLE);
                        bikeAdapter = new BikeAdapter(bikeList, getContext());
                        bikesRecyclerView.setAdapter(bikeAdapter);
                    }
                    Toast.makeText(getContext(), "Bikes loaded successfully!", Toast.LENGTH_SHORT).show();
                });
            }

            @Override
            public void onFailure(String errorMessage) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    noBikesTextView.setVisibility(View.VISIBLE);
                    bikesRecyclerView.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Error loading bikes: " + errorMessage, Toast.LENGTH_SHORT).show();
                });
            }
        });
    }
}
